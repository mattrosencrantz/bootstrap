# Run this script like:
# wget -O bootstrap https://bitbucket.org/mattrosencrantz/bootstrap/raw/HEAD/server.sh && bash bootstrap

set -e

apt-get update
apt-get upgrade -y
apt-get install -y git golang-go mercurial tmux emacs-nox haproxy nginx build-essential parallel unattended-upgrades

if test ! -e ~/.ssh; then
    ssh-keygen -f ~/.ssh/id_rsa -N ""
fi

read -p "Paste your public key: " PUBKEY
if test -n "$PUBKEY"; then
    # TODO: Test if $PUBKEY is already in authorized_keys.
    echo "$PUBKEY" >> ~/.ssh/authorized_keys
    chmod 600 ~/.ssh/authorized_keys
fi
echo "Navigate to https://bitbucket.org/mattrosencrantz/serverconfig/admin/deploy-keys/"
echo "and add:"
cat ~/.ssh/id_rsa.pub
read -p "Press enter when done."

if ! test -e serverconfig; then 
    git clone git@bitbucket.org:mattrosencrantz/serverconfig.git
else
    git -C serverconfig pull
fi

for NEWUSER in mattr rosencsd; do
    if ! id -u $NEWUSER >& /dev/null; then
        adduser $NEWUSER --disabled-password --gecos "" && adduser $NEWUSER sudo
    fi
done

source serverconfig/install.sh
