#!/usr/bin/env bash
# Run this script like:
# wget -O bootstrap https://bitbucket.org/mattrosencrantz/bootstrap/raw/HEAD/workstation.sh && bash bootstrap

set -e

read -p "Enter a tag for this machine (no spaces): " tag

sudo apt-get purge -y thunderbird* libreoffice* firefox*
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y git golang-go mercurial tmux emacs-nox build-essential parallel unattended-upgrades dkms linux-headers-$(uname -r) default-jdk

if ! dpkg -l google-chrome-stable; then
    # Get chrome, which can't be installed via apt-get.
    debname=google-chrome-stable_current_amd64.deb
    wget -O "/tmp/$debname" "https://dl.google.com/linux/direct/$debname"
    if ! sudo dpkg -i "/tmp/$debname"; then
        sudo apt-get -f install -y
    fi
fi

# TODO(mattr): Get and install Intellij.

echo "Please log into Chrome and sync your google account."
google-chrome >& /dev/null

if test ! -e ~/.ssh; then
    ssh-keygen -f ~/.ssh/id_rsa -N "" -C "$tag-$(date -I)"
fi
keyurl="https://bitbucket.org/account/user/mattrosencrantz/ssh-keys/"
echo "Navigate to $keyurl and add:"
cat ~/.ssh/id_rsa.pub
google-chrome $keyurl

repos=(configfiles bootstrap serverconfig)
for repo in ${repos[*]}; do
    if ! test -e $repo; then 
        git clone "git@bitbucket.org:mattrosencrantz/$repo.git"
    else
        git -C $repo pull
    fi
done

if ! grep "$(cat .ssh/id_rsa.pub)" configfiles/authorized_keys >& /dev/null; then
    cat .ssh/id_rsa.pub >> configfiles/authorized_keys
    git -C configfiles commit -am "Add new public key."
    git -C configfiles push
fi

source $HOME/configfiles/workstation.sh
source $HOME/configfiles/dotfiles.sh
source $HOME/.bashrc

# TODO(mattr): Set a cron job to pull configfiles and source
# workstation.sh periodically.

repodir="$HOME/go/src/bitbucket.org/mattrosencrantz/gotools"
mkdir -p $(dirname "$repodir")
if ! test -e "$repodir"; then 
    git clone "git@bitbucket.org:mattrosencrantz/gotools.git" "$repodir"
else
    git -C "$repodir" pull
fi
go install bitbucket.org/mattrosencrantz/gotools/tlayout

